import React from 'react';
import Card from '../Card';

function Cart({ data = [] }) {
  const full = (data) => {
    return data.map((data) => {
      return <Card data={data} delButCart={true} key={`${data.idMeal}`} />;
    });
  };
  const empty = () => {
    return <h1>No items to displey, please make your choice</h1>;
  };

  return (
    ((data === null || data.length === 0) && empty()) || (data && full(data))
  );
}

export default Cart;
