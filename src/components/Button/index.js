const Button = ({
  bgc = 'black',
  type = 'button',
  text = 'Button',
  btnClName = 'button',
  onClick = () => {},
}) => {
  return (
    <button
      type={type}
      onClick={() => {
        onClick();
      }}
      style={{
        backgroundColor: bgc,
      }}
      className={btnClName}
    >
      {text}
    </button>
  );
};

export default Button;
