function Headerlogo() {
  return (
    <div className='header-topmenu__logo'>
      <div className='header-topmenu__logo__icon'></div>
      <span className='header-topmenu__logo__text'>GoodFood</span>
    </div>
  );
}

export default Headerlogo;
