import '../Form/style.scss';
import React from 'react';
import Button from '../Button';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addDataFavorite } from '../../redux/actions';

function Form({ closeModalHandler = () => {} }) {
  const dispatch = useDispatch();

  const [value1, setValue1] = useState('');
  const [value2, setValue2] = useState('');

  const favoriteHandler = (id, data) => {
    let favorite = JSON.parse(localStorage.getItem('favorite') || '[]'); //получение данных массива favorite из Local storage (LS)
    if (!favorite.some((el) => el.idMeal === id)) {
      const newFavorit = [...favorite, data];
      //запись данных массива favorite в LS и Стору
      localStorage.setItem('favorite', JSON.stringify(newFavorit));
      dispatch(addDataFavorite(newFavorit));
    }
  };
  //генерация ID
  function genID(num) {
    return num + '' + Math.floor(Math.random() * 1000);
  }
  //генерация объекта данных внисимых пользователем
  function addItem(e) {
    e.preventDefault();
    let obj = {
      idMeal: genID(25),
      strMeal: value1,
      strInstructions: value2,
    };
    favoriteHandler(obj.idMeal, obj);
    closeModalHandler(); //закрытие модалки
  }

  return (
    <div className='form-wrapper'>
      <form className='form' onSubmit={addItem}>
        <div className='form__box'>
          <input
            value={value1}
            className='form__box__input'
            type='text'
            placeholder='Dish title'
            onChange={(event) => setValue1(event.target.value)} //считывание названия блюда
            required
          />
        </div>

        <div className='form__box'>
          <textarea
            value={value2}
            className='form__box__input'
            type='text'
            placeholder='Dish description...'
            onChange={(event) => setValue2(event.target.value)} //считывание инструкции блюда
            required
          ></textarea>
        </div>
        <div className='form__box'>
          <Button
            text='Add custom dish'
            type='submit'
            bgc='#8d81ac'
            btnClName='form__box__btn button'
          />
        </div>
      </form>
    </div>
  );
}

export default Form;
