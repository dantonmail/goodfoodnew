import '../Header/style.scss';
import Headerlogo from '../Headerlogo';
import Headernav from '../Headernav';
import Headerburger from '../Headerburger';

function Header() {
  return (
    <header className='header'>
      <div className='container container__header'>
        <div className='header-topmenu'>
          <Headerlogo />
          <Headerburger />
          <Headernav />
        </div>
      </div>
    </header>
  );
}
export default Header;
