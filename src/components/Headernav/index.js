import { NavLink } from 'react-router-dom';
const Nav = () => {
  return (
    <nav className='header-topmenu__nav'>
      <ul className='header-topmenu__list'>
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/'
            className='header-topmenu__link'
            activeClassName='header-topmenu__link__active'
          >
            Random dish
          </NavLink>
        </li>
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/favorite'
            className='header-topmenu__link'
            activeClassName='header-topmenu__link__active'
          >
            Favorite
          </NavLink>
        </li>
        <li className='header-topmenu__item'>
          <NavLink
            exact
            to='/cart'
            className='header-topmenu__link'
            activeClassName='header-topmenu__link__active'
          >
            Cart
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
