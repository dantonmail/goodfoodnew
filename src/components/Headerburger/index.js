function Headerburger() {
  return (
    <div className='header-topmenu__burger__wrapper'>
      <div className='header-topmenu__burger'>
        <span className='header-topmenu__burger__line'></span>
      </div>
    </div>
  );
}

export default Headerburger;
